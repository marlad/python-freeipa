# python-freeipa

Interface with the FreeIPA api

## Use

```
import freeipa

server = 'freeipa.example.com'
ca_bundle = '/etc/pki/tls/certs/ca-bundle.crt'

ipa = freeipa.ipa(idm_server, sslverify=ca_bundle)
allusers = ipa.user_find(nsaccountlock=False)
```
