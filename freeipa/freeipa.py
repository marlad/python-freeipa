# Based upon: https://github.com/nordnet/python-freeipa-json
#
#
import re
import json
import logging

import requests
from requests_kerberos import HTTPKerberosAuth, REQUIRED

__all__ = ['ipa']

# this is the apiversion of the IPA server
# this should only be changed if we are sure future versions support
# this syntax use "ipa ping" command to find api version of current ipa server.
apiversion = "2.228"


class ipa(object):

    def __init__(self, server, kerberos=False, sslverify=True):
        self.server = server
        self.sslverify = sslverify
        self.log = logging.getLogger(__name__)
        self.session = requests.Session()

        if kerberos:
            self.kerberos_login()

    def kerberos_login(self):
        ipa_login_url = f'https://{self.server}/ipa/session/login_kerberos'

        self.kerberos_auth = HTTPKerberosAuth(
                                mutual_authentication=REQUIRED,
                                sanitize_mutual_error_response=False)

        rv = requests.get(ipa_login_url,
                          auth=self.kerberos_auth,
                          verify=self.sslverify)

        if rv.status_code != 200:
            self.log.warning(f'Failed to log in to {self.server}, '
                             'retval={rv.status_code}')
            rv = None
        else:
            self.log.debug(f'Successfully logged in to {self.server}')
        return rv

    def makeReq(self, pdict):
        results = None
        ipaurl = f'https://{self.server}/ipa'
        session_url = f'{ipaurl}/session/json'
        method = pdict['method']

        header = {'referer': ipaurl,
                  'Content-Type': 'application/json',
                  'Accept': 'application/json'}

        data = {'id': 0,
                'method': method,
                'params': [pdict['item'], pdict['params']]}

        self.log.debug(f'Making {method} request to {1}',
                       session_url)

        request = self.session.post(session_url,
                                    headers=header,
                                    auth=self.kerberos_auth,
                                    data=json.dumps(data),
                                    verify=self.sslverify)

        results = request.json()

        if not results['result'] is None:
            if 'summary' in results['result']:
                self.log.debug(f"{method}: {results['result']['summary']}")
        else:
            if not results['error'] is None:
                self.log.warning(f"{method}: {results['error']['message']}")
        return results

    def host_find(self, hostname=None, in_hg=None, sizelimit=40000):

        m = {'method': 'host_find',
             'item': [hostname],
             'params': {'all': True,
                        'in_hostgroup': in_hg,
                        'sizelimit': sizelimit,
                        'version': apiversion}}

        results = self.makeReq(m)
        return results

    def host_exists(self, hostname=None):
        result = self.host_find(hostname)

        if not result['error'] and re.match('^1 host matched$',
                                            result['result']['summary']):
            self.log.debug('Host found in ipa')
            return True

        self.log.debug('Host not found in ipa')
        return False

    def host_mod(self, hostname, description=None,
                 location=None, platform=None, osver=None):

        m = {'item': [hostname],
             'method': 'host_mod',
             'params': {'all': True,
                        'description': description,
                        'nshostlocation': location,
                        'nshardwareplatform': platform,
                        'nsosversion': osver,
                        'version': apiversion}}

        self.log.debug(m)
        results = self.makeReq(m)
        return results

    def host_add(self, hostname, opasswd, description=None, force=True):

        m = {'item': [hostname],
             'method': 'host_add',
             'params': {'all': True,
                        'force': force,
                        'userpassword': opasswd,
                        'description': description,
                        'version': apiversion}}

        results = self.makeReq(m)
        return results

    def host_del(self, hostname):

        m = {'item': [hostname],
             'method': 'host_del',
             'params': {'version': apiversion}}

        results = self.makeReq(m)
        return results

    # Find all hostsgroups that contain a specified name
    def hostgroup_find(self, name=None, sizelimit=40000):

        m = {'method': 'hostgroup_find',
             'item': [name],
             'params': {'all': True,
                        'no_members': True,
                        'sizelimit': sizelimit,
                        'version': apiversion}}

        results = self.makeReq(m)
        return results

    # Find an exact matching hostgroup
    def hostgroup_findexact(self, name=None, sizelimit=40000):

        m = {'method': 'hostgroup_find',
             'item': [],
             'params': {'all': True,
                        'cn': name,
                        'no_members': True,
                        'sizelimit': sizelimit,
                        'version': apiversion}}

        results = self.makeReq(m)
        return results

    def hostgroup_exists(self, name=None):
        result = self.hostgroup_findexact(name)

        if not result['error'] and result['result']['count'] > 0:
            if result['result']['count'] == 1:
                self.log.debug('Hostgroup found in ipa')
                return result['result']['result'][0]['cn'][0]
            else:
                self.log.error(f'Error: More than 1 hostgroup matches '
                               'for: "{name}"')
        else:
            self.log.warning(f'Hostgroup {name} not found in ipa')
        return None

    def hostgroup_add_member(self, hostgroup=None, hostname=None):

        cn = self.hostgroup_exists(hostgroup)

        if(cn):
            # Check if host is already a member
            result = self.host_find(hostname=hostname, in_hg=str(cn))

            if not result['error'] and re.match('^0 hosts matched$',
                                                result['result']['summary']):
                m = {'method': 'hostgroup_add_member',
                     'item': [cn],
                     'params': {'host': [hostname],
                                'version': apiversion}}

                self.log.debug(m)
                results = self.makeReq(m)

                return results
            elif not result['error'] and re.match('^1 host matched$',
                                                  result['result']['summary']):
                self.log.info(f'Host is already member of hostgroup {cn}')
            else:
                self.log.error(
                    'hostgroup_add_member failed for unknown reason')
        else:
            self.log.error(f'Unknown hostgroup {hostgroup}')
        return None

    def user_find(self, username=None, sizelimit=40000, nsaccountlock=None):

        m = {'method': 'user_find',
             'item': [username],
             'params': {'all': True,
                        'sizelimit': sizelimit,
                        'version': apiversion,
                        'nsaccountlock': nsaccountlock}}

        results = self.makeReq(m)
        return results

    def user_mod(self, username=None, nsaccountlock=None):

        m = {'method': 'user_mod',
             'item': [username],
             'params': {'all': True,
                        'version': apiversion,
                        'nsaccountlock': nsaccountlock}}

        results = self.makeReq(m)
        return results

    def usergroup_find(self, usergroup=None, sizelimit=40000):
        m = {'method': 'group_find',
             'item': [usergroup],
             'params': {'all': True,
                        'nonposix': True,
                        'sizelimit': sizelimit,
                        'version': apiversion}}

        results = self.makeReq(m)
        return results

    def sudorule_find(self, sudorule=None, sizelimit=40000):
        m = {'method': 'sudorule_find',
             'item': [sudorule],
             'params': {'all': True,
                        'sizelimit': sizelimit,
                        'version': apiversion}}

        results = self.makeReq(m)
        return results

    def hbacrule_find(self, hbacrule=None, sizelimit=40000):
        m = {'method': 'hbacrule_find',
             'item': [hbacrule],
             'params': {'all': True,
                        'sizelimit': sizelimit,
                        'version': apiversion}}

        results = self.makeReq(m)
        return results